%global srcname importlib_resources
%global pkgname importlib-resources

Name:          python-%{pkgname}
Summary:       Read resources from Python packages
License:       ASL 2.0
URL:           http://importlib-resources.readthedocs.io/

Version:       1.0.2
Release:       1%{?dist}
Source0:       %{pypi_source}

BuildArch:     noarch


%global _description %{expand:
importlib_resources is a backport of Python 3.7’s standard library
importlib.resources module for Python 2.7, and 3.4 through 3.6. Users of
Python 3.7 and beyond should use the standard library module, since for these
versions, importlib_resources just delegates to that module.

The key goal of this module is to replace parts of pkg_resources with a
solution in Python’s stdlib that relies on well-defined APIs. This makes
reading resources included in packages easier, with more stable and consistent
semantics.}

%description %_description


%package -n python3-%{pkgname}
Summary:       %{summary}

BuildRequires: python3-devel
BuildRequires: %{py3_dist wheel}

%{?python_provide:%python_provide python3-%{pkgname}}


%description -n python3-%{pkgname} %_description


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py3_build


%install
%py3_install


%check
%{__python3} -m unittest discover


%files -n python3-%{pkgname}
%doc README.rst
%license LICENSE
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py*.egg-info


%changelog
* Sun Nov 03 2019 Mathieu Bridon <bochecha@daitauha.fr> - 1.0.2-1
- Initial package for Fedora.
